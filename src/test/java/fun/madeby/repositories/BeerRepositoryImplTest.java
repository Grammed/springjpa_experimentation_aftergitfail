package fun.madeby.repositories;

import fun.madeby.models.Beer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;


class BeerRepositoryImplTest {
BeerRepositoryImpl beerRepositoryImpl;
private EntityManagerFactory emf;
private EntityManager em;

@Mock
BeerRepository beerRepository;
@PersistenceUnit
public void setEmf(EntityManagerFactory emf){
	this.emf = emf;
	this.em = emf.createEntityManager();
}
@BeforeEach
void setUp() {
	beerRepository = Mockito.mock(BeerRepository.class);
	beerRepositoryImpl = new BeerRepositoryImpl();
}


@Test
void testFindById() {
	final int BEER_ID = 5;
	Beer beer = beerRepositoryImpl.findById(BEER_ID);
	System.out.println(beer.toString());
}
}