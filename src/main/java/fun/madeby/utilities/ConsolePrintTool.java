package fun.madeby.utilities;

/**
 * ConsolePrintTool
 * For special console printing needs
 *
 * @version 2.1.0 , 18-may-2021
 */
public final class ConsolePrintTool {
   static final boolean DEFAULT_TITLE_EMPTY_LINE_BEFORE = true;
   static final char DEFAULT_TITLE_TYPE = '-';

   private ConsolePrintTool() {
   }

   public static void printTitle(String title, char type, boolean emptyLineBefore) {
      if (title == null || title.length() == 0) return;

      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < title.length(); i++) {
         sb.append(type);
      }
      if (emptyLineBefore) System.out.println();
      System.out.println(title);
      System.out.println(sb.toString());
   }

   public static void printTitle(String title, char type) {
      printTitle(title, type, DEFAULT_TITLE_EMPTY_LINE_BEFORE);
   }

   public static void printTitle(String title) {
      printTitle(title, DEFAULT_TITLE_TYPE, DEFAULT_TITLE_EMPTY_LINE_BEFORE);
   }
}
