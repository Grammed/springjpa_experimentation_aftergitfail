package fun.madeby.logic;


import fun.madeby.models.Beer;
import fun.madeby.models.BeerOrder;
import fun.madeby.models.OrderItem;
import fun.madeby.repositories.*;

import java.sql.SQLOutput;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static fun.madeby.utilities.ConsoleInputTool.*;
import static fun.madeby.utilities.ConsolePrintTool.printTitle;


public final class BeerOrderManagement {

public static void beerOrderManagementMenu(BeerOrderRepository beerOrderRepositoryImpl,
                                           OrderItemRepository orderItemRepositoryImpl,
                                           BeerRepository beerRepositoryImpl) {
	beerRepositoryImpl = beerRepositoryImpl;
	beerOrderRepositoryImpl = beerOrderRepositoryImpl;
	orderItemRepositoryImpl = orderItemRepositoryImpl;



	boolean stay = true;
	while (stay) {
		printTitle("What would you like to do with Beer Orders?", '_');

		System.out.println("1. New beer order\n" +
			                   "2. Edit beer order\n" +
			                   "3. Delete beer order\n" +
			                   "4. List beer order's items\n" +
			                   "5. List all beer orders \n" +
			                   "6. List all beers by customer (stub)\n" +
			                   "0. exit");
		int choice = askUserInteger("Your choice: ", 0, 6);

		switch (choice) {
			case 1:
				boolean addOrderItems;
				BeerOrder beerOrder = new BeerOrder();
				Beer beer = new Beer();
				int amount;
				printTitle("Create new beer order: ", '+');
				beerOrder.setName(askUserString("Enter name for beer order: "));
				beerOrder = beerOrderRepositoryImpl.save(beerOrder);
				do {
					addOrderItems = askUserYesNoQuestion("Add order items?", true, true);
					if(addOrderItems) {
						printAllBeers(beerRepositoryImpl);
						beer = beerRepositoryImpl.findById(askUserInteger("Enter id of beer to order: "));
						amount = askUserInteger("How many" + beer.getName() + " of the " + beer.getStock() +
						" in stock, do you want to order?");
						OrderItem oi = new OrderItem(beerOrder, beer, amount);
						oi = orderItemRepositoryImpl.save(oi);
						System.out.println("Order item " + oi.getId() + " has been added to Beer Order " +
							                  oi.getBeerOrder().getId() + oi.getBeerOrder().getName());
						System.out.println("Adjusted stock...from --> " + beer.getStock());
						beer.setStock(beer.getStock() - oi.getAmount());
						beerRepositoryImpl.update(beer);
						System.out.println("to --> " + beer.getStock());
					}
				}while(addOrderItems);
				break;

			case 2:
				printTitle("Choose beer order to edit: ", '+');
				printAllBeerOrders(beerOrderRepositoryImpl);
				int OrderChoice = askUserInteger("Please enter id of the beer order you want to edit 0 to exit");
				if(OrderChoice != 0) {
					BeerOrder chosenBeerOrder = beerOrderRepositoryImpl.findById(OrderChoice);
					printBeerOrdersItems(chosenBeerOrder, orderItemRepositoryImpl);
				}




				break;
			case 3:
				System.out.println("DeleteBEEROrder");
				printTitle("Deleting beer created in this session ");
				break;
			case 4:
				printAllBeerOrders(beerOrderRepositoryImpl);
				break;
			case 5:
				printTitle("All beer orders", '+');
				break;
			case 6:
				printTitle("Beer orders by customer STUB: ", '+');
				break;
			case 0:
				stay = false;
				break;
			default:
				System.out.println("Please enter a valid option.");
		}

	}
}

private static List<Beer> printAllBeers(BeerRepository beerRepositoryImpl) {
	printTitle("All Beers, yes you will be able to search soon: ", '+');

	List<Beer> allBeers = (List<Beer>) beerRepositoryImpl.findAll();
	if(allBeers.size() != 0)
		for(Beer b: allBeers)
			System.out.println(b.getId() + " " + b.getName() + " x " + b.getStock() +
				                   " x " + b.getPrice());
	return allBeers;
}

private static List<OrderItem> printBeerOrdersItems(BeerOrder chosenBeerOrder,
                                                    OrderItemRepository orderItemRepositoryImpl) {
	printTitle("All Beer Orders Items: ", '+');

	List<OrderItem> allOrderItems = (List<OrderItem>) orderItemRepositoryImpl.findAllByBeerOrder(chosenBeerOrder);
	if(allOrderItems.size() != 0)
	for(OrderItem oi: allOrderItems)
		System.out.println(oi.getId() + " " + oi.getBeer().getName() + " x " + oi.getAmount() +
			                   " x " + oi.getBeer().getPrice() + " = " +
			                   (oi.getAmount() * oi.getBeer().getPrice()));
	return allOrderItems;
}

private static List<BeerOrder> printAllBeerOrders(BeerOrderRepository beerOrderRepositoryImpl) {
	printTitle("All Beer Orders: ", '+');

	List<BeerOrder> allBeerOrders = (List<BeerOrder>) beerOrderRepositoryImpl.findAll();
	for(BeerOrder bo: allBeerOrders)
		System.out.println(bo.getId() + " " + bo.getName());
	return allBeerOrders;
}
}




