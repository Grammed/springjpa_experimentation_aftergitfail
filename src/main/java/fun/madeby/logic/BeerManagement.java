package fun.madeby.logic;


import fun.madeby.models.Beer;
import fun.madeby.models.Brewer;
import fun.madeby.repositories.*;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static fun.madeby.utilities.ConsoleInputTool.askUserInteger;
import static fun.madeby.utilities.ConsoleInputTool.askUserString;
import static fun.madeby.utilities.ConsolePrintTool.printTitle;


public final class BeerManagement {
	// Just to save typing
private static final long CURRENT_NEW_BEER_BREWER = 99;
private static final int CURRENT_NEW_BEER_CATEGORY = 33;
private static final float CURRENT_NEW_BEER_ALCOHOL = 1.2f;
private static final double CURRENT_NEW_BEER_PRICE = 3;
private static final int CURRENT_NEW_BEER_STOCK = 1020;
// These are not plugged into ctx and so.. are .. use .. les.s..
//private static final BrewerRepository BREWER_REPO = new BrewerRepositoryImpl();
//private static final CategoryRepository CATEGORY_REPO = new CategoryRepositoryImpl();

public static void beerManagementMenu(BeerRepository beerRepositoryImpl,
                                      BrewerRepository brewerRepositoryImpl ,
                                      CategoryRepository categoryRepositoryImpl) {
	Beer createdBeer;
	long returnedForConfirmation = 0;

	boolean stay = true;
	while (stay) {
		printTitle("What would you like to do with Beers?", '_');

		System.out.println("1. Add a new beer\n" +
			                   "2. Edit beer stock\n" +
			                   "3. Delete existing beer\n" +
			                   "4. List all beers\n" +
			                   "5. List all beers by Brewer\n" +
			                   "6. List all beers by abv\n" +
			                   "0. exit");
		int choice = askUserInteger("Your choice: ", 0, 6);

		switch (choice) {
			case 1:
				printTitle("To test go through steps in order. Currently all new beers are added " +
					           "in an automated way, you can choose the name though.", '-');
				Beer beer = new Beer();
				beer.setName(askUserString("Enter beer name:"));
				beer.setBrewer(brewerRepositoryImpl.findById(CURRENT_NEW_BEER_BREWER));
				beer.setCategory(categoryRepositoryImpl.findById(CURRENT_NEW_BEER_CATEGORY));
				beer.setAlcohol(CURRENT_NEW_BEER_ALCOHOL);
				beer.setPrice(CURRENT_NEW_BEER_PRICE);
				beer.setStock(CURRENT_NEW_BEER_STOCK);
				returnedForConfirmation = beerRepositoryImpl.save(beer);

				printTitle("Testing new added beer saved and findByID() working:", '-');
				System.out.println(beerRepositoryImpl.findById(returnedForConfirmation));
				break;

			case 2:
				System.out.println("only works if you've created a beer first");
				printTitle("Editing the beer you just created, price 1.80 brewer 74");
				createdBeer = beerRepositoryImpl.findById(returnedForConfirmation);
				createdBeer.setPrice(1.80);
				createdBeer.setBrewer(brewerRepositoryImpl.findById(74));
				createdBeer.setStock(askUserInteger("Enter new stock amount:"));
				beerRepositoryImpl.update(createdBeer);

				printTitle("Testing new edited beer price now 1.80 brewer 74 and stock the " +
					           "amount you set working:", '-');
				System.out.println(beerRepositoryImpl.findById(returnedForConfirmation));
				break;
			case 3:
				System.out.println("only works if you've created a beer first");
				printTitle("Deleting beer created in this session " + returnedForConfirmation);
				beerRepositoryImpl.delete(returnedForConfirmation);
				break;
			case 4:
				printTitle("All beers in database:");
				Collection<Beer> allBeers = ((BeerRepository) beerRepositoryImpl).findAll();
				printTitle("Streamed", '+');
				System.out.println(allBeers.stream().map((Beer::getName)).collect(Collectors.toList()));
				List<Beer> beersList = (List<Beer>) allBeers;
				printTitle("List forEach", '-');
				for (Beer b : beersList)
					System.out.println(b.getId() + " " + b.getName());
				break;
			case 5:
				printTitle("All Brewers", '+');
				List<Brewer> allBrewers = (List<Brewer>) brewerRepositoryImpl.findAll();
				for (Brewer b : allBrewers)
					System.out.println(b.getId() + " " + b.getName());
				int brewerChoice = askUserInteger("Print all beers of which brewer? Choice: ");
				printTitle("All beers by brewer: " + brewerRepositoryImpl.findById(brewerChoice).getName());
				Collection<Beer> beersByBrewer = beerRepositoryImpl.findAllByBrewer(brewerRepositoryImpl.findById(brewerChoice));
				printTitle("Streamed", '+');
				System.out.println(beersByBrewer.stream().map((Beer::getName)).collect(Collectors.toList()));
				printTitle("\nList forEach", '-');
				for (Beer b : beersByBrewer)
					System.out.println(b.getId() + " " + b.getName() + " " + b.getBrewer().getName());
				break;
			case 6:
				printTitle("All beers by abv: ", '+');
				List<Beer> beersAtAbv = (List<Beer>) beerRepositoryImpl.findAllAtAbv(2);
				for (Beer b : beersAtAbv)
					System.out.println(b.getName() + " " + b.getBrewer() + " " + b.getAlcohol());
				break;
			case 0:
				stay = false;
				break;
			default:
				System.out.println("Please enter a valid option.");
		}

	}
}
}


