package fun.madeby;

import fun.madeby.models.BeerOrder;
import fun.madeby.models.OrderItem;
import fun.madeby.repositories.*;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import static fun.madeby.utilities.ConsoleInputTool.*;
import static fun.madeby.utilities.ConsolePrintTool.*;
import static fun.madeby.logic.BeerManagement.*;
import static fun.madeby.logic.BeerOrderManagement.*;



@SpringBootApplication
public class BeerApp {
ConfigurableApplicationContext ctx;
BeerRepositoryImpl beerRepositoryImpl;
BrewerRepositoryImpl brewerRepositoryImpl;
CategoryRepositoryImpl categoryRepositoryImpl;
BeerOrderRepositoryImpl beerOrderRepositoryImpl;
OrderItemRepositoryImpl orderItemRepositoryImpl;

public static void main(String[] args) {
	BeerApp app = new BeerApp();
	app.ctx = SpringApplication.run(BeerApp.class);
	app.beerRepositoryImpl = app.ctx.getBean(BeerRepositoryImpl.class);
	app.brewerRepositoryImpl = app.ctx.getBean(BrewerRepositoryImpl.class);
	app.categoryRepositoryImpl = app.ctx.getBean(CategoryRepositoryImpl.class);
	app.beerOrderRepositoryImpl = app.ctx.getBean(BeerOrderRepositoryImpl.class);
	app.orderItemRepositoryImpl = app.ctx.getBean(OrderItemRepositoryImpl.class);


	BeerOrder beerOrder = app.beerOrderRepositoryImpl.findById(1);
	OrderItem orderItem = app.orderItemRepositoryImpl.findById(1);
	/*Beer beer = app.beerRepositoryImpl.findById(4);
	System.out.println(beer.toString());
	beer.setName("WasAOC");
	app.beerRepositoryImpl.update(beer);
	System.out.println(app.beerRepositoryImpl.findById(4).toString());
	System.out.println(app.brewerRepositoryImpl.findById(1).toString());
	System.out.println(app.categoryRepositoryImpl.findById(2).toString());
	Brewer brewer = app.brewerRepositoryImpl.findById(1);
	Category category = app.categoryRepositoryImpl.findById(2);
	System.out.println("-------------------------------");


	Beer beerPersist = new Beer("Kumquat ale", brewer, category, 12.0, 1000, 8.8f);
	System.out.println(beerPersist);
	long id = app.beerRepositoryImpl.save(beerPersist);
	System.out.println("Beer " + id + " has been saved");
	System.out.println(beerRepositoryImpl.findById(beerRepositoryImpl.save(beerPersist)).toString());
	Collection<Beer> beerCollection3_8 = app.beerRepositoryImpl.findAllAtAbv(4f);
	System.out.println(beerCollection3_8.stream().map(Beer::getName).collect(Collectors.toList()));*/

	app.menuOne();
}

private void menuOne() {
	boolean menuOneActive = true;
	while (menuOneActive) {
		printTitle("Welcome to The Beer Department", '_');

		System.out.println("1. Beer Inventory Management\n" +
			                   "2. Brewer Management\n" +
			                   "3. Beer Order Management\n" +
			                   "4. Category Management\n" +
			                   "0. exit");
		int choice = askUserInteger("Your choice: ", 0, 4);
		switch (choice) {
			case 1:
					beerManagementMenu(beerRepositoryImpl, brewerRepositoryImpl, categoryRepositoryImpl);
				break;
			case 2:
				System.out.println("Brewer Management");
				break;
			case 3:
				beerOrderManagementMenu(beerOrderRepositoryImpl, orderItemRepositoryImpl, beerRepositoryImpl);
				break;
			case 4:
				System.out.println("Category Management");
				break;
			case 0:
			menuOneActive = false;
			break;
			default:
				System.out.println("PLease enter a valid option.");
		}

	}
}
}
