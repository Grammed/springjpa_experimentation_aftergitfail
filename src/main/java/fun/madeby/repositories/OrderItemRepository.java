package fun.madeby.repositories;

import fun.madeby.models.BeerOrder;
import fun.madeby.models.OrderItem;

import java.util.Collection;

public interface OrderItemRepository {

Collection<OrderItem> findAll();
OrderItem save(OrderItem orderItem);
OrderItem findById(long id);
void update(OrderItem orderItem);
void delete(OrderItem orderItem);
void delete(long id);
Collection<OrderItem> findAllByBeerOrder(BeerOrder beerOrder);

}
