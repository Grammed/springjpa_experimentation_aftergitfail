package fun.madeby.repositories;

import fun.madeby.models.BeerOrder;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Repository
public class BeerOrderRepositoryImpl implements  BeerOrderRepository{
private EntityManagerFactory emf;
private EntityManager em;

public EntityManager getEntityManager() { return em;}
public void setEntityManager(EntityManager em) {this.em = em;}
public EntityManagerFactory getEmf() {return emf;}

@PersistenceUnit
public void setEmf(EntityManagerFactory emf) {
	this.emf = emf;
	this.em = emf.createEntityManager();
}

@Override
public void delete(BeerOrder beerOrder) {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	em.remove(beerOrder);
	transaction.commit();

}

@Override
public void delete(long id) {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	BeerOrder beerOrder = findById(id);
	em.remove(beerOrder);
	transaction.commit();
}

@Override
public Collection<BeerOrder> findAll() {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	TypedQuery<BeerOrder> typedQuery = em.createQuery("SELECT bo FROM BeerOrder bo", BeerOrder.class);
	List<BeerOrder> allBeerOrders = typedQuery.getResultList();
	transaction.commit();
	return  allBeerOrders;
}

@Override
public BeerOrder findById(long id) {
	EntityTransaction transaction = em.getTransaction();
	transaction.begin();
	BeerOrder beerOrder = em.find(BeerOrder.class, id);
	transaction.commit();
	return beerOrder;
}

@Override
public BeerOrder save(BeerOrder beerOrder) {
	EntityTransaction tx = em.getTransaction();
	tx.begin();
	em.persist(beerOrder);
	tx.commit();
	return beerOrder;
}

@Override
public void update(BeerOrder beerOrder) {
	EntityTransaction tx = em.getTransaction();
	tx.begin();
	beerOrder  = em.merge(beerOrder);
	tx.commit();
	em.detach(beerOrder);
}
}
