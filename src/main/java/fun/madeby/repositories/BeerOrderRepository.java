package fun.madeby.repositories;

import fun.madeby.models.BeerOrder;
import java.util.Collection;

public interface BeerOrderRepository {

	void delete(BeerOrder beerOrder);
	void delete(long id);
	Collection<BeerOrder> findAll();
	BeerOrder findById(long id);
	BeerOrder save(BeerOrder beerOrder);
	void update(BeerOrder beerOrder);

}