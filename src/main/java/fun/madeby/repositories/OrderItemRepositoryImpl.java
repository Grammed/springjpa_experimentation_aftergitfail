package fun.madeby.repositories;


import fun.madeby.models.BeerOrder;
import fun.madeby.models.OrderItem;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Repository
public class OrderItemRepositoryImpl implements OrderItemRepository {

private EntityManagerFactory emf;
private EntityManager em;

public EntityManager getEntityManager() {
	return em;
}

public void setEntityManager(EntityManager em) {
	this.em = em;
}

public EntityManagerFactory getEmf(){
	return emf;
}

@PersistenceUnit
public void setEmf(EntityManagerFactory emf){
	this.emf = emf;
	this.em = emf.createEntityManager();
}


@Override
public Collection<OrderItem> findAll() {
	/*EntityTransaction tx = em.getTransaction();
	tx.begin();
	TypedQuery<OrderItem> typedQuery = em.createQuery("SELECT oi FROM OrderItem oi", OrderItem.class);
	List<OrderItem> allOrderItems = typedQuery.getResultList();
	return allOrderItems;*/
	return null;
}

@Override
public OrderItem save(OrderItem orderItem) {
	EntityTransaction  tx = em.getTransaction();
	tx.begin();
	em.persist(orderItem);
	tx.commit();
	return orderItem;


}

@Override
public OrderItem findById(long id) {
	return null;
}

@Override
public void update(OrderItem orderItem) {

}

@Override
public void delete(OrderItem orderItem) {

}

@Override
public void delete(long id) {

}

@Override
public Collection<OrderItem> findAllByBeerOrder(BeerOrder beerOrder) {
	EntityTransaction tx = em.getTransaction();
	tx.begin();
	TypedQuery<OrderItem> typedQuery = em.createQuery("SELECT oi FROM OrderItem oi WHERE oi.beerOrder=:beerOrder", OrderItem.class);
	typedQuery.setParameter("beerOrder", beerOrder);
	Collection<OrderItem> beerOrdersItems = typedQuery.getResultList();
	tx.commit();
	return beerOrdersItems;
}
}
