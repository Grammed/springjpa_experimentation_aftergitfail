package fun.madeby.models;
import javax.persistence.*;
import java.util.Objects;

@Entity
public class OrderItem {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@ManyToOne //No cascade
	@JoinColumn(name = "OrderId", columnDefinition = "BIGINT NOT NULL")
	private BeerOrder beerOrder;
	@OneToOne
	@JoinColumn(name = "Beer_id", columnDefinition = "BIGINT NOT NULL")
	private Beer beer;
	@Column(name = "Amount", columnDefinition = "INT(11) NOT NULL ")
	private int amount;

public OrderItem() {
}

public OrderItem(BeerOrder beerOrder, Beer beer, int amount) {
	this.beerOrder = beerOrder;
	this.beer = beer;
	this.amount = amount;
}

public int getId() {
	return id;
}

public BeerOrder getBeerOrder() {
	return beerOrder;
}

public void setBeerOrder(BeerOrder beerOrder) {
	this.beerOrder = beerOrder;
}

public Beer getBeer() {
	return beer;
}

public void setBeer(Beer beer) {
	this.beer = beer;
}

public int getAmount() {
	return amount;
}

public void setAmount(int amount) {
	this.amount = amount;
}

@Override
public boolean equals(Object o) {
	if (this == o) return true;
	if (o == null || getClass() != o.getClass()) return false;
	OrderItem orderItem = (OrderItem) o;
	return id == orderItem.id;
}

@Override
public int hashCode() {
	return Objects.hash(id);
}

@Override
public String toString() {
	return "OrderItem{" +
		       "id=" + id +
		       ", beerOrder Id: " + beerOrder.getId() +
		       ", beerOrder Name: " + beerOrder.getId() +
		       ", beer=" + beer.getName() +
		       ", amount=" + amount +
		       '}';
}
}
