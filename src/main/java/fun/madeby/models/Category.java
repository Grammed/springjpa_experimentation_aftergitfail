package fun.madeby.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
//import java.util.stream.Collectors;

@Entity
public class Category {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private int id;
@Column(name = "Category", columnDefinition = "VARCHAR(50) NOT NULL UNIQUE")
private String name;
@OneToMany(mappedBy = "category", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH})
private Set<Beer> beers;

public Category() {
}

public Category(String name) {
	this(name, new HashSet<>());
}

public Category(String name, HashSet<Beer> beers) {
	this.name = name;
	this.beers = beers;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public Set<Beer> getBeers() {
	return beers;
}

public void setBeers(Set<Beer> beers) {
	this.beers = beers;
}

@Override
public boolean equals(Object o) {
	if (this == o) return true;
	if (o == null || getClass() != o.getClass()) return false;
	Category category = (Category) o;
	return id == category.id;
}

@Override
public int hashCode() {
	return Objects.hash(id);
}

@Override
public String toString() {
	return "Category{" +
		       "id=" + id +
		       ", Category NAME='" + name + '\'' +
		       ", Category BEERS=" +// beers.stream().map(b -> b.getName()).collect(Collectors.toList()) +
		       '}';
}
}
