package fun.madeby.models;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class BeerOrder {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "Name", columnDefinition = "VARCHAR(50) NOT NULL")
	private String name;
	@OneToMany(mappedBy = "beerOrder", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH})
	private Set<OrderItem> orderItems;

public BeerOrder() {
}

public BeerOrder(String name, Set<OrderItem> orderItems) {
	this.name = name;
	this.orderItems = orderItems;
}

public BeerOrder(String name) {
	this.name = name;
	this.orderItems = new HashSet<>();
}

public long getId() {
	return id;
}

public void setId(long id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public Set<OrderItem> getOrderItems() {
	return orderItems;
}

public void setOrderItems(Set<OrderItem> orderItems) {
	this.orderItems = orderItems;
}

@Override
public boolean equals(Object o) {
	if (this == o) return true;
	if (o == null || getClass() != o.getClass()) return false;
	BeerOrder beerOrder = (BeerOrder) o;
	return id == beerOrder.id;
}

@Override
public int hashCode() {
	return Objects.hash(id);
}

@Override
public String toString() {
	return "BeerOrder{" +
		       "id=" + id +
		       ", name='" + name + '\'' +
		       ", orderItems=" +  orderItems + //orderItems.stream().map(OrderItem::getId).collect(Collectors.toList());
		       '}';
}
}
