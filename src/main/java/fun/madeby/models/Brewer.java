package fun.madeby.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "Brewers")
public class Brewer implements Serializable {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "Id", unique = true)
private long id;
@Column(name = "Name", length = 50, nullable = false)
private String name;
@Column(name = "Address", length = 255, nullable = false)
private String address;
@Column(name = "ZipCode", length = 255, nullable = false)
private String zipCode;
@Column(name = "City", length = 255, nullable = false)
private String city;
@Column(name = "Turnover", nullable = false, columnDefinition = "int default 0")
private int turnover;
@OneToMany(mappedBy = "brewer", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH})
private Set<Beer> beers = new HashSet<>();

// Removed for now for simplicity and for step by stem implementation This info does not exist in the
// brewers table, it is obtained and set from BrewerId foreign key in the Beers table
/*@OneToMany(mappedBy = "brewer", cascade = {CascadeType.PERSIST, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH})
private Set<Beer> beers = new HashSet<>();*/

public Brewer() {
}

public Brewer(String name, String address, String zipCode, String city, int turnover) {
	this(name, address, zipCode, city, turnover, new HashSet<>());
}

public Brewer(String name, String address, String zipCode, String city, int turnover, Set<Beer> beers) {
	this.name = name;
	this.address = address;
	this.zipCode = zipCode;
	this.city = city;
	this.turnover = turnover;
	this.beers = beers;
}

public long getId() {
	return id;
}

public void setId(long id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getZipCode() {
	return zipCode;
}

public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}

public int getTurnover() {
	return turnover;
}

public void setTurnover(int turnover) {
	this.turnover = turnover;
}

public Set<Beer> getBeers() {
	return beers;
}

public void setBeers(Set<Beer> beers) {
	this.beers = beers;
}

@Override //more than my understanding of this magically returning and printing a collection of beers, the related Beers are
// populated during the find beer by id query in the original. As is category.
public String toString() {
	return String.format("%d: %s%n Address: %s, %s %s%nTurnover: %d%nBrews:,", // %s%n",
		getId(),
		getName(),
		getAddress(),
		getZipCode(),
		getCity(),
		getTurnover()
		//getBeers().stream().map(Beer::getName).collect(Collectors.toList())
	);
}

}